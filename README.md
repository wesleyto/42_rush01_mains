# README #

### What is this repository for? ###

* 42 rush01 sanity checks

### Notes ###

* These are some example sudokus (including the one givien in the PDF).
* They are all solvable, unless otherwise indicated (error).

### How do I get set up? ###

* Copy the files to your project directory (the outer dir, not 'ex00').
* i.e. if your structure is 'rush01/ex00', copy everything to 'rush01'
* Run the included script to check norm, compile, and run the test cases.
* NO GUARANTEES.
* EXAMINE YOUR OUTPUT! The script DOES NOT check your output for correctness.

### Who do I talk to? ###

* Repo owner
* Deity/deities of your choice