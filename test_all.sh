LAST_EX=14

norminette -R CheckForbiddenSourceHeader ex00/*

gcc -Wall -Wextra -Werror -o ex00/rush-1 ex00/*.c

for num in $(seq -f "%02g" 0 $LAST_EX)
	do
		echo
		echo ==============================
		echo ===== Testing Example $num =====
		echo ==============================
		cat EXAMPLES/example$num* | xargs ./ex00/rush-1
	done

rm ex00/rush-1